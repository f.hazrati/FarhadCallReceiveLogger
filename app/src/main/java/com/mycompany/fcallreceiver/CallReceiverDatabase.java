package com.mycompany.fcallreceiver;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class CallReceiverDatabase extends SQLiteOpenHelper {

    String CreateTableSql = "" +
            "CREATE TABLE callLogs(" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT ," +
            "phoneNumber TEXT , " +
            "ContentData TEXT " +
            ")";

    public CallReceiverDatabase(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CreateTableSql);
    }

    public void insert(String phoneNumbr,String ContentData) {
        SQLiteDatabase db = this.getWritableDatabase();
        String q = "INSERT INTO callLogs(phoneNumber,ContentData)" +
                "VALUES('" + phoneNumbr + "','" + ContentData + "')";
        db.execSQL(q);
        db.close();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}

